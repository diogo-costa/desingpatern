package design.patern.service.impl;

import design.patern.model.Address;
import design.patern.model.Client;
import design.patern.repository.AddressRepository;
import design.patern.repository.ClientRepository;
import design.patern.service.ClientService;
import design.patern.service.ViaCepService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ViaCepService viaCepService;

    @Override
    public Iterable<Client> searchAll() {
        return clientRepository.findAll();
    }

    @Override
    public Client findById(Long id) {

        Optional<Client> client = clientRepository.findById(id);
        return client.get();
    }

    @Override
    public void insert(Client client) {

        clientSaveWithZipCode(client);
    }

    @Override
    public void update(Long id, Client client) {

        Optional<Client> clientDb = clientRepository.findById(id);

        if (clientDb.isPresent()) {

            clientSaveWithZipCode(client);
        }
    }

    @Override
    public void delete(Long id) {

        clientRepository.deleteById(id);
    }

    private void clientSaveWithZipCode(Client client) {

        String zipCode = client.getAddress().getZipCode();
        Address address = addressRepository.findById(zipCode).orElseGet(
            () -> {
                Address newAddress = viaCepService.consultZipCode(zipCode);
                addressRepository.save(newAddress);
                return newAddress;
            }
        );

        client.setAddress(address);

        clientRepository.save(client);
    }
}
