package design.patern.service;


import design.patern.model.Client;

public interface ClientService {

    Iterable<Client> searchAll();

    Client findById(Long id);

    void insert(Client client);

    void update(Long id, Client client);

    void delete(Long id);

}
