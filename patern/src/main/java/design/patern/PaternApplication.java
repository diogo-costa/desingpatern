package design.patern;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class PaternApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaternApplication.class, args);
	}

}
