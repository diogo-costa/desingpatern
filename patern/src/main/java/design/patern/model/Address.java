package design.patern.model;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    @Id
    private String zipCode;
    private String street;
    private String complement;
    private String neighborhood;
    private String locale;
    private String uf;
    private String ibge;
    private String gia;
    private String ddd;
    private String siafi;

}
